<?php

namespace Drupal\dynamic_layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;

/**
 * Provides special class for dynamic layout.
 */
class DynamicLayout extends LayoutDefault {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'attach_libraries' => '',
      'container_class' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['container_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Container class'),
      '#default_value' => $configuration['container_class'],
    ];

    $form['regions'] = [
      '#type' => 'details',
      '#title' => $this->t('Regions'),
      '#tree' => TRUE,
      '#open' => TRUE,
      '#attributes' => [
        'id' => 'regions-list',
      ],
    ];

    if (!$form_state->get('regions')) {
      $form_state->set('regions', $configuration['regions'] ?? [
        ['class' => ''],
      ]);
    }
    foreach ($form_state->get('regions') as $delta => $region) {
      $form['regions'][$delta] = [
        '#type' => 'fieldset',
      ];
      $form['regions'][$delta]['class'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Classes for :delta', [':delta' => $delta]),
        '#default_value' => $region['class'],
      ];
    }
    $form['add_region'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add region'),
      '#submit' => [
        'callback' => [$this, 'addRegion'],
      ],
      '#ajax' => [
        'callback' => [$this, 'addRegionCallback'],
        'wrapper' => 'regions-list',
      ],
    ];

    $form['attach_libraries'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Attach libraries'),
      '#description' => $this->t('Input every library from new line. "module_name(theme_name)/library_name"'),
      '#default_value' => $configuration['attach_libraries'],
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * The ajax submit click handler.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function addRegion(array $form, FormStateInterface $form_state): void {
    $extra_regions = $form_state->get('regions');
    $extra_regions[] = ['class' => ''];
    $form_state
      ->set('regions', $extra_regions)
      ->setRebuild();
  }

  /**
   * The ajax callback.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element.
   */
  public static function addRegionCallback(array $form, FormStateInterface $form_state): array {
    return $form['layout_settings']['regions'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['attach_libraries'] = $form_state->getValue('attach_libraries');
    $this->configuration['regions'] = \array_filter($form_state->getValue('regions'), static function ($item) {
      return !empty($item['class']);
    });
    $this->configuration['container_class'] = $form_state->getValue('container_class');
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    $attach_libraries = \explode("\n", $this->configuration['attach_libraries']);
    $attach_libraries = \array_map('trim', $attach_libraries);
    $attach_libraries = \array_filter($attach_libraries, 'strlen');
    foreach ($attach_libraries as $attach_library) {
      $build['#attached']['library'][] = $attach_library;
    }
    return $build;
  }

  /**
   * Set regions to this layout instance.
   */
  protected function setRegions(): void {
    if (empty($this->configuration['regions'])) {
      return;
    }

    $regions = [];
    foreach ($this->configuration['regions'] as $delta => $region) {
      $regions[$delta]['label'] = $this->t('Region :delta', [':delta' => $delta]);
    }
    $this->getPluginDefinition()->setRegions($regions);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    $regions = [];
    foreach ($this->configuration['regions'] as $delta => $region) {
      $regions[$delta]['label'] = $this->t('Region :delta', [':delta' => $delta]);
    }
    $this->pluginDefinition->setRegions($regions);
    return $this->pluginDefinition;
  }

}
