<?php

/**
 * @file
 * Defines base theme hooks.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for "dynamic_layout" theme.
 *
 * Default template: dynamic-layout.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: A source data element.
 */
function template_preprocess_dynamic_layout(array &$variables): void {
  if (isset($variables['content']['#settings'])) {
    $variables['settings'] = $variables['content']['#settings'];
  }
  foreach (Element::children($variables['content']) as $child) {
    $variables['regions'][$child] = $variables['content'][$child];
    if (!isset($variables['content'][$child]['#attributes'])) {
      $variables['content'][$child]['#attributes'] = [];
    }
    $variables['region_attributes'][$child] = new Attribute($variables['content'][$child]['#attributes']);
  }
  $variables['container_class'] = $variables['settings']['container_class'] ?? 'layout';
}
